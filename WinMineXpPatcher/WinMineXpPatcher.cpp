// WinMineXpPatcher.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#define WINMINE_PROC_NAME ("../Binaries/Winmine__XP.exe")

void reload_board(HANDLE hProcess)
{
	SIZE_T bytesRead = 0;

	LPCVOID xBoxMacAddr = (LPCVOID)0x01005334;
	LPCVOID yBoxMacAddr = (LPCVOID)0x01005338;
	LPCVOID rgBlkAddr = (LPCVOID)(0x01005340);

	UINT32 xBoxMac = 0;
	UINT32 yBoxMac = 0;

	ReadProcessMemory(hProcess, xBoxMacAddr, &xBoxMac, sizeof(xBoxMac), &bytesRead);
	ReadProcessMemory(hProcess, yBoxMacAddr, &yBoxMac, sizeof(yBoxMac), &bytesRead);

	UINT8 *rgBlk = (UINT8 *)malloc(0x360);

	ReadProcessMemory(hProcess, rgBlkAddr, rgBlk, 0x360, &bytesRead);

	if (bytesRead < 0x360) {
		printf("ERROR READING BOARD DATA\n");
		return;
	}

	unsigned long i = 0;
	unsigned long j = 0;

	for (i = 1; i <= yBoxMac; ++i) {
		for (j = 1; j <= xBoxMac; ++j) {
			// printf("%.2X", rgBlk[i*xBoxMac + j]);
			UINT8 cell = rgBlk[(i << 5) + j];
			if (cell & 0x80)
			{
				// Mine!
				printf(" * ");
			}
			else
			{
				// Safe cell
				if (cell >= 0x40) {
					// Has a number
					printf(" %c ", cell - 0x10);
				}
				else {
					printf(" - ");
				}
				
			}
		}
		printf("\n");
	}
}

int main()
{
	STARTUPINFOA si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Start the child process. 
	if (!CreateProcessA(NULL,   // No module name (use command line)
		WINMINE_PROC_NAME,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		return 1;
	}

	while (1) {
		reload_board(pi.hProcess);
		printf("PRESS RETURN KEY IN ORDER TO RELOAD GAME BOARD...\n");
		getc(stdin);
	}

	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

    return 0;
}

